#!/bin/bash

apt-get update
apt-get install qt4-default
cd ..

git clone https://gitlab.com/railundergrads/tcd_rviz_plugins.git
git clone https://github.com/NickL77/RPLidar_Hector_SLAM.git

cd ..
source devel/setup.bash
catkin_make
chmod 666 /dev/ttyUSB0

