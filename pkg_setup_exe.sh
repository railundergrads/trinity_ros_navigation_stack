#!/bin/bash

apt-get update

apt-get -y install python3-pip ros-melodic-navigation ros-melodic-openslam-gmapping ros-melodic-rplidar-ros ros-melodic-hector-slam qt4-default ros-melodic-sensor-msgs ros-melodic-tf ros-melodic-rviz curl ros-melodic-rqt ros-melodic-rqt-common-plugins ros-melodic-rqt-robot-plugins ros-melodic-image-geometry ros-melodic-gazebo-ros-pkgs ros-melodic-gazebo-ros-control
sudo apt-get install ros-melodic-move-base* -y
sudo apt-get install ros-melodic-map-server* -y
sudo apt-get install ros-melodic-amcl* -y
sudo apt-get install ros-melodic-navigation* -y
cd ..

git clone https://gitlab.com/railundergrads/tcd_rviz_plugins.git
git clone https://github.com/NickL77/RPLidar_Hector_SLAM.git
git clone https://github.com/turtlebot/turtlebot_simulator.git
git clone https://github.com/turtlebot/turtlebot.git
git clone https://github.com/turtlebot/turtlebot_apps.git
git clone https://github.com/turtlebot/turtlebot_msgs.git
git clone https://github.com/turtlebot/turtlebot_interactions.git

git clone https://github.com/toeklk/orocos-bayesian-filtering.git
cd orocos-bayesian-filtering/orocos_bfl/
./configure
make
sudo make install
cd ../
make
cd ../

git clone https://github.com/udacity/robot_pose_ekf
git clone https://github.com/ros-perception/depthimage_to_laserscan.git

git clone https://github.com/yujinrobot/kobuki_msgs.git
git clone https://github.com/yujinrobot/kobuki_desktop.git
cd kobuki_desktop/
rm -r kobuki_qtestsuite
cd -
git clone --single-branch --branch melodic https://github.com/yujinrobot/kobuki.git
mv kobuki/kobuki_description kobuki/kobuki_bumper2pc \
  kobuki/kobuki_node kobuki/kobuki_keyop \
  kobuki/kobuki_safety_controller ./

git clone https://github.com/yujinrobot/yujin_ocs.git
mv yujin_ocs/yocs_cmd_vel_mux yujin_ocs/yocs_controllers .
rm -rf yujin_ocs

sudo apt-get install ros-melodic-kobuki-* -y
sudo apt-get install ros-melodic-ecl-streams -y
sudo apt-get install ros-melodic-yocs-velocity-smoother -y

cd ..
source devel/setup.bash
catkin_make
chmod 666 /dev/ttyUSB0


#Turtlebot specific
apt-get install -y ros-melodic-move-base* ros-melodic-map-server* ros-melodic-amcl* ros-melodic-navigation* ros-melodic-joy*
