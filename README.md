# Trinity ROS Navigation Stack

# Installation:
- Clone this repo into catkin workspace source folder
`git clone git@gitlab.com:railundergrads/trinity_ros_navigation_stack.git`
## For installing package to work with turtlebot:
- Run `./pkg_setup__exe.sh` to install all packages required to control and map with the turtlebot and rviz plugin
- Use roslaunch to launch with hector slam
`roslaunch trinity_ros_navigation_stack hector_slam_2d_mapping.launch`
- Once Rviz loads launch the tcd_rviz Navigation panel using the Panels-> Add new panel and selecting the Navigation panel
- When the plugin loads in use cmd_vel_mux/input/teleop as your topic path to control the turtlebot

## For installing packages to work with custom robot (using own teleoperations and bringups)
- Run `./minimal_install.sh` to install packages for RPLidar, Hector SLAM and the Trinity Rviz Plugin
- Open launch/hector_slam_2d_mapping.launch and replace `<include file="$(find turtlebot_bringup)/launch/minimal.launch" />` with your robot's bringup node or launch file
- Use roslaunch to launch with hector slam
`roslaunch trinity_ros_navigation_stack hector_slam_2d_mapping.launch`
- Once Rviz loads launch the tcd_rviz Navigation panel using the Panels-> Add new panel and selecting the Navigation panel
- When the plugin loads in use cmd_vel_mux/input/teleop as your topic path to control the turtlebot

## Controls
The keyboard controls for the project can be seen here

![Alt text](https://gitlab.com/railundergrads/tcd_rviz_plugins/-/blob/main/images/Controls.png?raw=true "Keyboard mapping")

## Saving a map
To save the map (default location will be /maps) run the command
`rostopic pub syscommand std_msgs/String "savegeotiff"`

## Notes
This repo currently only supports 2D mapping using RPLidar. 


### Issues
If you experience issues with RPLidar node, run `ls -l /dev |grep ttyUSB` and check which serial path the RPLidar is mapped to (default is ttyUSB0) and the authority of the port. You can then run `sudo chmod 666 /dev/ttyUSBx` where x is the number for the USB port.
